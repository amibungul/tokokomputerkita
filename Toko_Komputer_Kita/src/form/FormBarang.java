/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import koneksi.konek;

/**
 *
 * @author User
 */
public class FormBarang extends javax.swing.JFrame {
public DefaultTableModel model;
    Statement st;
    ResultSet rs;
    Connection con;
    /**
     * Creates new form FormBarang
     */
    public FormBarang() {
        initComponents();
        konek DB = new konek();
        DB.config();
        con = (com.mysql.jdbc.Connection) DB.con;
        st = DB.stm;
        txtIDBar.setEnabled(false);
        model = new DefaultTableModel();
        this.tblbarang.setModel(model);
        model.addColumn("Id Barang");
        model.addColumn("Nama Barang");
        model.addColumn("Stok");
        model.addColumn("Harga");
        loadDataBarang();
        IdData();
    }
    
    public void TambahData(){   
   String id = txtIDBar.getText();
   String nama = txtNamaBar.getText();
   String stok = txtStokBar.getText();
   String harga = txtHarrgaBar.getText();
   try{  
     String sql="Insert into barang (id_barang,nama_barang,stok,harga)"+
             "values (?,?,?,?)";  
       try (PreparedStatement p = (PreparedStatement)con.prepareStatement(sql)) {
           p.setString(1,id);
           p.setString(2,nama);
           p.setString(3,stok);
           p.setString(4,harga);
           p.executeUpdate();
       }  
   }catch(SQLException e){  
    System.out.println(e);  
   }finally{  
    loadDataBarang();  
    JOptionPane.showMessageDialog(this,"Data Telah Tersimpan");  
  }  
 }
    
    public void IdData(){
     try{  
     String sql = "select * from barang order by id_barang desc";
     st = con.createStatement();
     rs = st.executeQuery(sql);
     if(rs.next()){
         String awal = rs.getString("id_barang").substring(1);
         String akhir = "" + (Integer.parseInt(awal)+1);
         String nol = "";
         if (akhir.length()==1){
             nol = "000";
         }
         else if(akhir.length()==2){
             nol = "00";
         }
         else if (akhir.length()==3){
             nol = "0";
         }
         else if (akhir.length()==4){
             nol = "";
         }
         txtIDBar.setText("B"+nol+akhir);
     }
     else{
         txtIDBar.setText("B0001");
     }
     }
     catch (Exception e){
         JOptionPane.showMessageDialog(null, e);
     }
   }
    
    public void DeleteData(){  
   int i =tblbarang.getSelectedRow();  
   if(i==-1)  
   { 
       return;
   }  
   String KODE=(String)model.getValueAt(i, 0);  
   try {   
     String sql="DELETE From barang WHERE id_barang=?";  
       try (PreparedStatement p = (PreparedStatement)con.prepareStatement(sql)) {
           p.setString(1,KODE);
           p.executeUpdate();
       }  
   }catch(SQLException e){  
     System.out.println("Terjadi Kesalahan");  
   }finally{  
     loadDataBarang();  
     JOptionPane.showMessageDialog(this,"Sukses Hapus Data...");  
   }  
 } 
    
    public void UpdateData(){  
   int i= tblbarang.getSelectedRow();  
     if(i==-1)  
     {  
       return;  
     }  
         String id = txtIDBar.getText();
         String nama = txtNamaBar.getText();
         String stok = txtStokBar.getText();
         String harga = txtHarrgaBar.getText();
     try{  
       String sql;  
       sql = "update barang set nama_barang=?,stok=?,harga=? WHERE id_barang=?";
       try (PreparedStatement p = (PreparedStatement)con.prepareStatement(sql)) {
           p.setString(1,nama);
           p.setString(2,stok);
           p.setString(3,harga);
           p.setString(4,id);
           p.executeUpdate();
       }  
     }catch(SQLException e){  
       System.out.println("Terjadi Kesalahan");  
     }finally{  
       loadDataBarang();  
       JOptionPane.showMessageDialog(this,"Data Telah Diubah");  
     }  
 }
    private void loadDataBarang(){
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        try{
            String sql = "select * from barang";
            rs = st.executeQuery(sql);
            while(rs.next()){
                Object[] obj = new Object[4];
                obj[0] = rs.getString("id_barang");
                obj[1] = rs.getString("nama_barang");
                obj[2] = rs.getString("stok");
                obj[3] = rs.getString("harga");
                model.addRow(obj);
            }
        }
        catch(SQLException err){
            JOptionPane.showMessageDialog(null, err.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtIDBar = new javax.swing.JTextField();
        txtNamaBar = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtStokBar = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtHarrgaBar = new javax.swing.JTextField();
        btnedit = new javax.swing.JButton();
        btnbaru = new javax.swing.JButton();
        btnsimpan = new javax.swing.JButton();
        btnhapus = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblbarang = new javax.swing.JTable();
        btntambah1 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("ID BARANG");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 70, -1, -1));

        jLabel7.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("NAMA BARANG");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 130, -1, -1));
        getContentPane().add(txtIDBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 90, 110, -1));
        getContentPane().add(txtNamaBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 150, 110, -1));

        jLabel8.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("STOK BARANG");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 70, -1, -1));
        getContentPane().add(txtStokBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 90, 110, -1));

        jLabel9.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("HARGA BARANG");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 130, -1, -1));
        getContentPane().add(txtHarrgaBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 150, 110, -1));

        btnedit.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        btnedit.setForeground(new java.awt.Color(0, 102, 0));
        btnedit.setText("EDIT");
        btnedit.setBorder(null);
        btnedit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btneditActionPerformed(evt);
            }
        });
        getContentPane().add(btnedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 150, 70, 30));

        btnbaru.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        btnbaru.setForeground(new java.awt.Color(0, 102, 0));
        btnbaru.setText("BARU");
        btnbaru.setBorder(null);
        btnbaru.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbaruActionPerformed(evt);
            }
        });
        getContentPane().add(btnbaru, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 50, 70, 30));

        btnsimpan.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        btnsimpan.setForeground(new java.awt.Color(0, 102, 0));
        btnsimpan.setText("SIMPAN");
        btnsimpan.setBorder(null);
        btnsimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsimpanActionPerformed(evt);
            }
        });
        getContentPane().add(btnsimpan, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 200, 70, 30));

        btnhapus.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        btnhapus.setForeground(new java.awt.Color(0, 102, 0));
        btnhapus.setText("HAPUS");
        btnhapus.setBorder(null);
        btnhapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnhapusActionPerformed(evt);
            }
        });
        getContentPane().add(btnhapus, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 250, 70, 30));

        jLabel1.setFont(new java.awt.Font("Trajan Pro 3", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(153, 255, 255));
        jLabel1.setText("TOKO KOMPUTER KITA");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 20, -1, -1));

        tblbarang.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "id_barang", "nama_barang", "stok", "harga"
            }
        ));
        jScrollPane2.setViewportView(tblbarang);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 190, -1, 220));

        btntambah1.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        btntambah1.setForeground(new java.awt.Color(0, 102, 0));
        btntambah1.setText("TAMBAH");
        btntambah1.setBorder(null);
        btntambah1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntambah1ActionPerformed(evt);
            }
        });
        getContentPane().add(btntambah1, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 100, 70, 30));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/WhatsApp Image 2018-10-24 at 21.33.15.jpeg"))); // NOI18N
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 630, 420));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnbaruActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbaruActionPerformed
        // TODO add your handling code here:
        txtNamaBar.setText("");
        txtStokBar.setText("");
        txtHarrgaBar.setText("");
    }//GEN-LAST:event_btnbaruActionPerformed

    private void btnsimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsimpanActionPerformed
        // TODO add your handling code here:
        this.UpdateData();
    }//GEN-LAST:event_btnsimpanActionPerformed

    private void btneditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btneditActionPerformed
        // TODO add your handling code here:
        try{
            int row = tblbarang.getSelectedRow();
            String id = tblbarang.getValueAt(row, 0).toString();
            String nama = tblbarang.getValueAt(row, 1).toString();
            String stok = tblbarang.getValueAt(row, 2).toString();
            String harga = tblbarang.getValueAt(row, 3).toString();
            txtIDBar.setText(String.valueOf(id));
            txtNamaBar.setText(String.valueOf(nama));
            txtStokBar.setText(String.valueOf(stok));
            txtHarrgaBar.setText(String.valueOf(harga));
        }
        catch (Exception e){
        }
    }//GEN-LAST:event_btneditActionPerformed

    private void btnhapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnhapusActionPerformed
        // TODO add your handling code here:
        this.DeleteData();
    }//GEN-LAST:event_btnhapusActionPerformed

    private void btntambah1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntambah1ActionPerformed
        // TODO add your handling code here:
        IdData();
        this.TambahData();
    }//GEN-LAST:event_btntambah1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormBarang.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormBarang.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormBarang.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormBarang.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormBarang().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnbaru;
    private javax.swing.JButton btnedit;
    private javax.swing.JButton btnhapus;
    private javax.swing.JButton btnsimpan;
    private javax.swing.JButton btntambah1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblbarang;
    private javax.swing.JTextField txtHarrgaBar;
    private javax.swing.JTextField txtIDBar;
    private javax.swing.JTextField txtNamaBar;
    private javax.swing.JTextField txtStokBar;
    // End of variables declaration//GEN-END:variables
}
