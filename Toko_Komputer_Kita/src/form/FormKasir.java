/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author User
 */
public class FormKasir extends javax.swing.JFrame {
public DefaultTableModel model;
    Statement st;
    ResultSet rs;
    Connection con;
  
    /**
     * Creates new form FormBarang
     */
    public FormKasir() {
        initComponents();
        
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtIDBar = new javax.swing.JTextField();
        txtNamaBar = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtHarSa = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtTotHar = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtTunai = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtKemb = new javax.swing.JTextField();
        txtJumBar = new javax.swing.JTextField();
        btnproses = new javax.swing.JButton();
        btnbayar = new javax.swing.JButton();
        btnhapus = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblkasir = new javax.swing.JTable();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("ID BARANG");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 70, -1, -1));

        jLabel7.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("NAMA BARANG");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 120, -1, -1));
        getContentPane().add(txtIDBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 90, 110, -1));
        getContentPane().add(txtNamaBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 140, 110, -1));

        jLabel8.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("HARGA SATUAN");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 70, -1, -1));
        getContentPane().add(txtHarSa, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 90, 110, -1));

        jLabel9.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("UANG TUNAI");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 220, -1, -1));

        jLabel11.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("TOTAL HARGA");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 170, -1, -1));
        getContentPane().add(txtTotHar, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 190, 110, -1));

        jLabel12.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("KEMBALIAN");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 280, -1, -1));
        getContentPane().add(txtTunai, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 240, 110, -1));

        jLabel13.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("JUMLAH BARANG");
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 120, -1, -1));
        getContentPane().add(txtKemb, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 300, 110, -1));
        getContentPane().add(txtJumBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 140, 110, -1));

        btnproses.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        btnproses.setForeground(new java.awt.Color(0, 102, 0));
        btnproses.setText("PROSES");
        btnproses.setBorder(null);
        btnproses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnprosesActionPerformed(evt);
            }
        });
        getContentPane().add(btnproses, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 180, 80, 40));

        btnbayar.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        btnbayar.setForeground(new java.awt.Color(0, 102, 0));
        btnbayar.setText("BAYAR");
        btnbayar.setBorder(null);
        btnbayar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbayarActionPerformed(evt);
            }
        });
        getContentPane().add(btnbayar, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 280, 80, 40));

        btnhapus.setFont(new java.awt.Font("Sitka Small", 1, 11)); // NOI18N
        btnhapus.setForeground(new java.awt.Color(0, 102, 0));
        btnhapus.setText("HAPUS");
        btnhapus.setBorder(null);
        btnhapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnhapusActionPerformed(evt);
            }
        });
        getContentPane().add(btnhapus, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 230, 80, 40));

        jLabel1.setFont(new java.awt.Font("Trajan Pro 3", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(153, 255, 255));
        jLabel1.setText("TOKO KOMPUTER KITA");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 20, -1, -1));

        tblkasir.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "no_pemb", "nama_barang", "tgl_pemb", "kode_supplier", "total_pemb"
            }
        ));
        jScrollPane2.setViewportView(tblkasir);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 340, 590, 200));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/WhatsApp Image 2018-10-24 at 21.33.15.jpeg"))); // NOI18N
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 630, 570));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnprosesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnprosesActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_btnprosesActionPerformed

    private void btnbayarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbayarActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_btnbayarActionPerformed

    private void btnhapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnhapusActionPerformed
        // TODO add your handling code here:
     
    }//GEN-LAST:event_btnhapusActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormKasir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormKasir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormKasir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormKasir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormKasir().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnbayar;
    private javax.swing.JButton btnhapus;
    private javax.swing.JButton btnproses;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblkasir;
    private javax.swing.JTextField txtHarSa;
    private javax.swing.JTextField txtIDBar;
    private javax.swing.JTextField txtJumBar;
    private javax.swing.JTextField txtKemb;
    private javax.swing.JTextField txtNamaBar;
    private javax.swing.JTextField txtTotHar;
    private javax.swing.JTextField txtTunai;
    // End of variables declaration//GEN-END:variables
}
