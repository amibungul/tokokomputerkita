/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import javax.swing.JOptionPane;

/**
 *
 * @author Sera
 */
public class FormUtama extends javax.swing.JFrame {

    /**
     * Creates new form FormUtama
     */
    public FormUtama() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnPelanggan = new javax.swing.JButton();
        btnKasir = new javax.swing.JButton();
        btnKaryawan = new javax.swing.JButton();
        btnBarang = new javax.swing.JButton();
        btnLaporan = new javax.swing.JButton();
        btnSupplier = new javax.swing.JButton();
        btnPengeluaran = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnPelanggan.setBackground(new java.awt.Color(0, 153, 153));
        btnPelanggan.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnPelanggan.setForeground(new java.awt.Color(0, 153, 153));
        btnPelanggan.setText("PELANGGAN");
        btnPelanggan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPelangganActionPerformed(evt);
            }
        });
        getContentPane().add(btnPelanggan, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 390, 290, 80));

        btnKasir.setBackground(new java.awt.Color(0, 153, 153));
        btnKasir.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnKasir.setForeground(new java.awt.Color(0, 153, 153));
        btnKasir.setText("K A S I R");
        btnKasir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKasirActionPerformed(evt);
            }
        });
        getContentPane().add(btnKasir, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 190, 290, 80));

        btnKaryawan.setBackground(new java.awt.Color(0, 153, 153));
        btnKaryawan.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnKaryawan.setForeground(new java.awt.Color(0, 153, 153));
        btnKaryawan.setText("KARYAWAN");
        btnKaryawan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKaryawanActionPerformed(evt);
            }
        });
        getContentPane().add(btnKaryawan, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 190, 290, 80));

        btnBarang.setBackground(new java.awt.Color(0, 153, 153));
        btnBarang.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnBarang.setForeground(new java.awt.Color(0, 153, 153));
        btnBarang.setText("B A R A N G");
        btnBarang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBarangActionPerformed(evt);
            }
        });
        getContentPane().add(btnBarang, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 190, 290, 80));

        btnLaporan.setBackground(new java.awt.Color(0, 153, 153));
        btnLaporan.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnLaporan.setForeground(new java.awt.Color(0, 153, 153));
        btnLaporan.setText("L A P O R A N");
        btnLaporan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLaporanActionPerformed(evt);
            }
        });
        getContentPane().add(btnLaporan, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 540, 290, 80));

        btnSupplier.setBackground(new java.awt.Color(0, 153, 153));
        btnSupplier.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnSupplier.setForeground(new java.awt.Color(0, 153, 153));
        btnSupplier.setText("SUPPLIER");
        btnSupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSupplierActionPerformed(evt);
            }
        });
        getContentPane().add(btnSupplier, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 390, 290, 80));

        btnPengeluaran.setBackground(new java.awt.Color(0, 153, 153));
        btnPengeluaran.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        btnPengeluaran.setForeground(new java.awt.Color(0, 153, 153));
        btnPengeluaran.setText("P E N G E L U A R A N");
        btnPengeluaran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPengeluaranActionPerformed(evt);
            }
        });
        getContentPane().add(btnPengeluaran, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 390, 290, 80));

        jPanel1.setBackground(new java.awt.Color(60, 160, 160));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1190, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 280, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 190, 1190, 280));

        jPanel2.setBackground(new java.awt.Color(60, 160, 160));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 290, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 150, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 470, 290, 150));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/form utama.jpeg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1280, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnKasirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKasirActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Ini Kasir");
    }//GEN-LAST:event_btnKasirActionPerformed

    private void btnBarangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBarangActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Ini Barang");
    }//GEN-LAST:event_btnBarangActionPerformed

    private void btnKaryawanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKaryawanActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Ini Karyawan");
    }//GEN-LAST:event_btnKaryawanActionPerformed

    private void btnPengeluaranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPengeluaranActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Ini Pengeluaran");
    }//GEN-LAST:event_btnPengeluaranActionPerformed

    private void btnSupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSupplierActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Ini Supplier");
    }//GEN-LAST:event_btnSupplierActionPerformed

    private void btnPelangganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPelangganActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Ini Pelanggan");
    }//GEN-LAST:event_btnPelangganActionPerformed

    private void btnLaporanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLaporanActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Ini Laporan");
    }//GEN-LAST:event_btnLaporanActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormUtama().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBarang;
    private javax.swing.JButton btnKaryawan;
    private javax.swing.JButton btnKasir;
    private javax.swing.JButton btnLaporan;
    private javax.swing.JButton btnPelanggan;
    private javax.swing.JButton btnPengeluaran;
    private javax.swing.JButton btnSupplier;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}
